
describe('Exercicio 1', function (){
    describe('Soma de todos os números múltiplos de 3 ou 5 de números naturais abaixo de 1000', function (){
        describe('teste multiplo 3', function (){
        
            var number;
            it('deve ser multiplo de 3',function(){
                number=6;
                expect(multipleOf3(number)).toBe(true);
            })
            
            it('não deve ser multiplo de 3',function(){

            number=4;
                expect(multipleOf3(number)).not.toBe(true);
            })
        });
        describe('teste multiplo de 5', function (){
        
            var number;
            
            it('deve ser multiplo de 5',function(){
                number=5;
                expect(multipleOf5(number)).toBe(true);
            })
            it('não deve ser multiplo de 5',function(){
                number=4;
                expect(multipleOf5(number)).not.toBe(true);
            })
        });
        describe('teste multiplo de 3 ou 5', function (){
        
            var number;
            
            it('3 deve ser multiplo de 3 ou 5',function(){
                number=3;
                expect(multipleOf3Or5(number)).toBe(true);
            })
            it('5 deve ser multiplo de 3 ou 5',function(){
                number=5;
                expect(multipleOf3Or5(number)).toBe(true);
            })
            it('4 não deve ser multiplo de 3 ou 5',function(){
                number=4;
                expect(multipleOf3Or5(number)).not.toBe(true);
            })
        });
        describe('soma dos numeros multiplos de 3 ou 5', function (){
        
            var number;
            var result;
            it('soma de multiplos de 3 ou 5 de  0 a 10 deve  resultar 23',function(){
                result=23;
                number=10;
                expect(getSum3Or5(number)).toBe(result);
            });

            it('soma de multiplos de 3 ou 5 de  0 a 1000 deve  resultar 233168',function(){
                result=233168;
                number=1000;
                expect(getSum3Or5(number)).toBe(result);
            });
            it('soma de multiplos de 3 ou 5 de  0 a 15 não deve  resultar 23',function(){
                result=23;
                number=15;
                expect(getSum3Or5(number)).not.toBe(result);
            });
            it('soma de multiplos de 3 ou 5 de  0 a 1005 não deve  resultar 233168',function(){
                result=233168;
                number=1005;
                expect(getSum3Or5(number)).not.toBe(result);
            });
        });
    });
    describe('Soma de todos os números múltiplos de 3 e 5 de números naturais abaixo de 1000', function (){

        describe('teste multiplo de 3 e 5', function (){
        
            var number;
            
            it('15 deve ser multiplo de 3 e 5',function(){
                number=15;
                expect(multipleOf3And5(number)).toBe(true);
            })
            it('30 deve ser multiplo de 3 e 5',function(){
                number=30;
                expect(multipleOf3And5(number)).toBe(true);
            })
            it('13 não deve ser multiplo de 3 ou 5',function(){
                number=13;
                expect(multipleOf3And5(number)).not.toBe(true);
            })
        });


        describe('soma dos numeros multiplos de 3 e 5', function (){
        
            var number;
            var result;
            it('soma de multiplos de 3 e 5 de  0 a 30 deve  resultar 15',function(){
                result=15;
                number=30;
                expect(getSum3And5(number)).toBe(result);
            });

            it('soma de multiplos de 3 ou 5 de  0 a 1000 deve  resultar 33165',function(){
                result=33165;
                number=1000;
                expect(getSum3And5(number)).toBe(result);
            });
            it('soma de multiplos de 3 e 5 de  0 a 10 não deve  resultar 15',function(){
                result=15;
                number=10;
                expect(getSum3And5(number)).not.toBe(result);
            });
            it('soma de multiplos de 3 e 5 de  0 a 1015 não deve  resultar 33165',function(){
                result=33165;
                number=1015;
                expect(getSum3And5(number)).not.toBe(result);
            });
        });
    });
    describe('Soma de todos os números múltiplos de (3 ou 5) e 7de números naturais abaixo de 1000', function (){


        describe('teste multiplo 7', function (){
        
            var number;
            it('7 deve ser multiplo de 7',function(){
                number=7;
                expect(multipleOf7(number)).toBe(true);
            })
            
            it('4 não deve ser multiplo de 7',function(){

            number=4;
                expect(multipleOf7(number)).not.toBe(true);
            })
        });

        describe('teste multiplo de 3 ou 5 e 7', function (){
        
            var number;
            
            it('21 deve ser multiplo de 3 ou 5 e 7' ,function(){
                number=21;
                expect(multipleOf3Or5And7(number)).toBe(true);
            })
            it('35 deve ser multiplo de 3 ou 5 e 7' ,function(){
                number=35;
                expect(multipleOf3Or5And7(number)).toBe(true);
            })
            
            it('13 não deve ser multiplo de 3 ou 5',function(){
                number=13;
                expect(multipleOf3And5(number)).not.toBe(true);
            })
        });

        describe('soma dos numeros multiplos de 3 ou 5 e 7', function (){
        
            var number;
            var result;
            it('soma de multiplos de 3 ou 5 e 7 de  0 a 23 deve  resultar 21',function(){
                result=21;
                number=23;
                expect(getSum3Or5And7(number)).toBe(result);
            });

            it('soma de multiplos de 3 ou 5 e 7 de  0 a 1000 deve  resultar 33173',function(){
                result=33173;
                number=1000;
                expect(getSum3Or5And7(number)).toBe(result);
            });
            it('soma de multiplos de 3 ou 5 e 7 de  0 a 20 não deve  resultar 21',function(){
                result=21;
                number=20;
                expect(getSum3Or5And7(number)).not.toBe(result);
            });
            it('soma de multiplos de 3 ou 5 e 7 de  0 a 1025 não deve  resultar 33173',function(){
                result=33173;
                number=1025;
                expect(getSum3Or5And7(number)).not.toBe(result);
            });
        });
    });
});