
describe('Exercicio 4', function (){
    
    afterAll(()=>{
        products=[];
     })

    describe('Teste dos produtos', function (){

        let prod=new Product('geladeira',2400);
        let tipo=(typeof prod);

        it('deve ser um objeto',function(){
            expect(tipo ).toBe('object');
        });
        
        it('deve ter um nome',function(){
            expect(prod.name).not.toBe(null || undefined ||'');
        });
        
        it('o nome deve ser o definido',function(){
            expect(prod.name).toBe('geladeira');
        });
        
        it('deve ter um preço',function(){
            expect(prod.price).not.toBe(null || undefined ||'');
        });
        
        it('o preço deve ser o definido',function(){
            expect(prod.price).toBe(2400);
        });
        
    });


    describe('Teste dos itens do carrinho', function (){

        let prod=new Product('geladeira',2400);
        let item=new ItemKart(prod,3)
        let tipo=(typeof item);

        it('deve ser um objeto',function(){
            expect(tipo ).toBe('object');
        });
        
        it('deve ter um produto',function(){
            expect(item.product).not.toBe(null || undefined ||'');
        });
        
        it('deve ter uma quantidade definida',function(){
            expect(item.qtd).not.toBe(null || undefined ||'');
        });
        it('a quantidade deve ser adefinida',function(){
            expect(item.qtd).toBe(3);
        });
        
        it('o produto deve ser o informado',function(){
            expect(item.product).toBe(prod);
        });
        
    });

    describe('Teste do user', function (){

        let user=new User('Matheus',84145000);
        let tipo=(typeof user);

        it('deve ser um objeto',function(){
            expect(tipo ).toBe('object');
        })
        
        it('deve ter um nome',function(){
            expect(user.nome).not.toBe(null || undefined ||'');
        });
        it('deve ter um endereço',function(){
            expect(user.address).not.toBe(null || undefined ||'');
        });

        it('deve ter um carrinho atrelado',function(){
            expect(user.kart).not.toBe(null || undefined ||'');
        });

        it('o nome deve ser o definido',function(){
            expect(user.name).toBe('Matheus');
        });
        it('o endereço deve ser o definido',function(){
            expect(user.address).toBe(84145000);
        });
        
        


        let prod=new Product('geladeira',2400);
        user.addItem(prod);

        it('deve adicionar o produto no carrinho',function(){
            expect(user.kart[0].product).toBe(prod);
        });


    });

    describe('Teste do calculo do total', function (){

        let user=new User('Matheus',84145000);
        let prod=new Product('geladeira',2400);
        user.addItem(prod);

        let total=getTotal(user);

        it('deve retornar dois valores',function(){

            expect(total).not.toBe(false);
            expect(typeof total).toBe('object');
            expect(total.length).toBe(3);

        });

        it('deve retornar os valores fornecidos',function(){
            expect(total[0]).toBe(2400);
            expect(total[1]).toBe(0);
            expect(total[2]).toBe(2400);
        });


        it('deve retornar os novos valores fornecidos',function(){
            let user=new User('Marcos',22001000);
            let prod=new Product('T-shirt',85);
            user.addItem(prod);

            let total=getTotal(user);
            expect(total[0]).toBe(85);
            expect(total[1]).toBe(50);
            expect(total[2]).toBe(135);
        });
    });
});