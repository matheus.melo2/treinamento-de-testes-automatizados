
let products=[];

class Product {
    constructor (name,price){
        this.name=name;
        this.price=price;
    }
}

class ItemKart {
    constructor (product,qtd){
        this.product=product;
        this.qtd=qtd;
    }
}

class User {
    constructor(name, address) {
        this.name = name;
        this.address = address;
        this.kart=[];
        this.addItem=(product,qtd=1)=>{
            let prod=new ItemKart(product,qtd);
            this.kart.push(prod);
        };
    }
}


function getTotal(user){
    if(user.kart.length==0){
        return false;
    }else{
        let totalProducts=0;
        let totalFrieght=0;
        user.kart.forEach((item)=>{
            totalProducts+=item.product.price*item.qtd;
            let freight=0;
            if(item.product.price<100){
                freight=requestCorreios(user.cep,item.qtd);
            }
            totalFrieght+=freight;
        })

        return [totalProducts,totalFrieght,totalFrieght+totalProducts];
    }
}

function requestCorreios(cep,qtd){
    return 50*qtd;
}