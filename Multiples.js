function multipleOf3(val){
    return val%3===0;
}
function multipleOf5(val){
    return val%5===0;
}

function multipleOf7(val){
    return val%7===0;
}

function multipleOf3Or5(val){
    return(multipleOf3(val)|| multipleOf5(val));
}
function getSum3Or5(val){
    let result=0;
    for(let i=0;i<val;i++){
        if(multipleOf3Or5(i)){
            result+=i;
        }
    }
    return(result);
}

function multipleOf3And5(val){
    return(multipleOf3(val) && multipleOf5(val));
}
function getSum3And5(val){
    let result=0;
    for(let i=0;i<val;i++){
        if(multipleOf3And5(i)){
            result+=i;
        }
    }H
    return(result);
}

function multipleOf3Or5And7(val){
    return((multipleOf3(val) || multipleOf5(val)) && multipleOf7(val));
}

function getSum3Or5And7(val){
    let result=0;
    for(let i=0;i<val;i++){
        if(multipleOf3Or5And7(i)){
            result+=i;
        }
    }
    return(result);
}


function readableMultiple(number){
    if(multipleOf3Or5(number)){
        return('é um  múltiplo de 3 ou 5');
    }else{
        return('não é um múltiplo de 3 ou 5');

    }
}