
describe('Exercicio 2', function (){
    var result='';
    beforeAll(()=>{
        result=window.prompt('Informe um valor para ser testado:');
    });
    describe('valida numero', function (){
        
        // alert(result);
        it('input  não deve ser nulo',function(){
            expect(result).not.toBe(null);
        });
        it('input  deve ser um numero',function(){
            // result=window.prompt('Informe um valor para ser testado:');
            expect(isNaN(result)).not.toBe(true);
        });
    });

    describe('Teste da função se o numero é feliz', function (){
        
        it('7  deve resultar em 1',function(){
            expect(happy('7')).toBe(true);
        });
        
        it('8 não deve resultar em 1',function(){
            expect(happy('8')).not.toBe(true);
        });
        
    });

    afterAll(()=>{
        alert(happy(result));
    });
        


});