const data=new Date();
const Categoria={
    PADRAO:0,
    ESPECIAL:1
};
const valoresDeMultas=[
    {
        '<=5':2,
        '>5':5,
    },{
        '<=5':5,
        '>5':9,
    }
];
class Livro {
    constructor(obj) {
        this.nome = obj.deNome;
        this.categoria=obj.comCategoria;
    }
}
class Leitor {
    constructor(obj){
        this.nome=obj.deNome;
        this.sobrenome=obj.sobrenome;
        this.idade=obj.idade;
        this.locacoes=obj.comLocacoes;
    }
}
class Locacao {
    constructor(obj) {
        this.livro = obj.doLivro;
        this.dataLocacao=new Date(obj.retiradoNaData);
        this.vencimento=calcularVencimento(this);
        if(obj.dataDevolucao===undefined || obj.dataDevolucao === null){
            this.foiDevolvido=false;
            this.estaVencido=this.vencimento<new Date();
            if(this.estaVencido){
                this.deltaDias=calcularDeltaDias(this);
                this.valorMulta=calcularMulta(this);
            }
        }else{
            this.foiDevolvido=true;
            this.dataDevolucao=obj.dataDevolucao;
        }
    }
}
function subtrair(valor1,valor2){
    return(Math.abs(valor1-valor2));
}
function emMilisegundos(dias){
    return Math.floor(dias*(1000 * 60 * 60  * 24));
}
function emDias(milisSegundos){
    return Math.floor(milisSegundos/(1000 * 60 * 60  * 24));
}
function calcularDeltaDias(locacao){
    var deltaMilisSegundos = subtrair(locacao.vencimento.getTime(), data.getTime());
    return emDias(deltaMilisSegundos);
}
function valorDaMultaPorDia(locacao,valorDiario){
    if(locacao.deltaDias<=5){
        return valorDiario['<=5'];
    }else{
        return valorDiario['>5'];
    }
}
function calcularMulta(locacao){
    const valoresPorCategoria=valoresDeMultas[locacao.livro.categoria];
    const valorDaMulta=locacao.deltaDias*valorDaMultaPorDia(locacao,valoresPorCategoria);
    return(valorDaMulta);
}
function prazoDeLocacao(locacao){
    const prazoDeAcordoCom=[5,3]
    const aCategoriaDoLivro=locacao.livro.categoria;
    return(prazoDeAcordoCom[aCategoriaDoLivro]);
}
function calcularVencimento(locacao){
    const vencimentoEmMilisSegundos=locacao.dataLocacao.getTime()+emMilisegundos(prazoDeLocacao(locacao));
    const vencimentoEmData=new Date(vencimentoEmMilisSegundos);
    return(vencimentoEmData);
}
function calcularMultadoLeitor(leitor){
    let multa=0;
    leitor.locacoes.forEach((locacao)=>{
        if(locacao.valorMulta!=undefined){
            multa+=locacao.valorMulta;
        }
    });
    return (multa);
}