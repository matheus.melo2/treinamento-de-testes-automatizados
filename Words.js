
let words='0abcdefghijklmnopqrstuvxywzABCDEFGHIJKLMNOPQRSTUVXYWZ'
function getNumber(letter){
    return words.indexOf(letter);
}
function convertWord(word){
    let total=0;
    for(let i=0;i<word.length;i++){
        total+=parseInt(getNumber(word.substr(i,1)));
    }
    return total;
}

function isPrimary(number){
    let i=2;
    let result=0;
    while(result!=1 && i !=number && number%i!==0){
        result=number/i;
        i++;
    }
    return(i==number);
}

function readablePrimary(number){
    if(isPrimary(number)){
        return('é um número primo');
    }else{
        return('não é um número primo');

    }
}