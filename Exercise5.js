describe('Exercicio 5', ()=>{
    let livro=new Livro({
        deNome:'Livro teste',
        comCategoria:Categoria.ESPECIAL,
    });
    it('o nome e categoria do livro deve ser igual as fornecidas',()=>{
        
        expect(livro.nome).toBe('Livro teste');
        expect(livro.categoria).toBe(Categoria.ESPECIAL);
    });
    it('1 dia deve ser 86400000 milissegundos',()=>{
        expect(emMilisegundos(1)).toBe(86400000);
    });
    it('86400000 milissegundos  deve ser 1 dia ',()=>{
        expect(emDias(86400000)).toBe(1);
    });
    it('subtrair 2 de 3 deve ser 1',()=>{
        expect(subtrair(3,2)).toBe(1);
    });
    describe('Testando leitor e locações', ()=>{
        let locacao=new Locacao({
            doLivro:livro,
            retiradoNaData:'12/15/2019',   
        })
        it('o livro da locação deve ser o definido anterioromente',()=>{
            expect(locacao.livro).toBe(livro);
        });
        it('a data de locação deve ser 15/12/2019',()=>{
            expect(locacao.dataLocacao).toEqual(new Date('12/15/2019'));
        });
        it('a data de vencimento deve ser 18/12/2019',()=>{
            expect(locacao.vencimento).toEqual(new Date('12/18/2019'));
        });
        it('o livro deve estar marcado como não devolvido',()=>{
            expect(locacao.foiDevolvido).toBe(false);
        });
        it('a locação deve estar vencida',()=>{
            expect(locacao.estaVencido).toBe(true);
        });        
        it('a multa deve ser de R$ 5,00',()=>{
            expect(locacao.valorMulta).toBe(5);
        });        
    });
    it('A multa total deste leitor deve ser de 24', ()=>{
        const multaTotal=calcularMultadoLeitor(
            new Leitor({
                deNome:'matheus',
                sobrenome:'Santos',
                idade:'24',
                comLocacoes:[
                    new Locacao({
                        doLivro:new Livro({
                            deNome:'Cronicas de Gelo e Fogo',
                            comCategoria:Categoria.PADRAO,
                        }),
                        retiradoNaData:'12/15/2019',   
                    }),
                    new Locacao({
                        doLivro:new Livro({
                            deNome:'SQPR',
                            comCategoria:Categoria.ESPECIAL,
                        }),
                        retiradoNaData:'12/12/2019',     
                    }),
                    new Locacao({
                        doLivro:new Livro({
                            deNome:'O Principe',
                            comCategoria:Categoria.PADRAO,
                        }),
                        retiradoNaData:'12/12/2019',     
                    }),
                ]
            })
        );
        expect(multaTotal).toBe(24);

    });
});