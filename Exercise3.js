
describe('Exercicio 3', function (){
    var result='';
    beforeAll(()=>{
        result=window.prompt('Informe uma palavra para ser testada:');
    });
    
    describe('Teste da conversão dos numeros', function (){
        it('testa se b é 2',function(){
            expect(getNumber('b') ).toBe(2);
        });
        
        it('testa se z é 26',function(){
            expect(getNumber('z') ).toBe(26);
        });
        it('testa se Z é 52',function(){
            expect(getNumber('Z') ).toBe(52);
        });
        
        it('Teste abc é 6', function (){
            expect(convertWord('abc')).toBe(6);
        });
    });

    describe('Teste da função de numeros primos', function (){
        it('2 deve ser primo',function(){
            expect(isPrimary(2) ).toBe(true);
        });
        it('20 não deve ser primo',function(){
            expect(isPrimary(20) ).not.toBe(true);
        });
        it('19 deve ser primo',function(){
            expect(isPrimary(19) ).toBe(true);
        });
    });
    afterAll(()=>{
        let number=convertWord(result);
        alert('o resultado da conta é: '+number+' este numero '+readablePrimary(number)+', '+readableHappy(number)+', '+readableMultiple(number));
    });
        


});